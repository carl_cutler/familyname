from pyramid.view import view_config
from .models import MyModel
from pyramid.response import Response

@view_config(context=MyModel, renderer='templates/mytemplate.pt')
def my_view(request):
    return {'project': 'familyname'}
    

def hello_world(request):
    return Response('Hello there!')