from pyramid.config import Configurator
from pyramid_zodbconn import get_connection
from .models import appmaker
from . import views
import os

def root_factory(request):
    conn = get_connection(request)
    return appmaker(conn.root())


    
def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(root_factory=root_factory, settings=settings)
    print(settings)
    config.include('pyramid_chameleon')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('hello', '/getstarted')
    config.add_view(views.hello_world, route_name='hello')
    
    config.scan()
    host = os.getenv('IP', '0.0.0.0')
    port = os.getenv('PORT', '8080')
    return config.make_wsgi_app()
